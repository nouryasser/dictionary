#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node {
	char word[50]; // data
	int c; // 1-red, 0-black
	struct node* p; // parent
	struct node* r; 
	struct node* l;
};

struct node* root = NULL;

struct node* bstInsert(struct node* root1,
					struct node* temp)
{
	if (root1 == NULL)
		return temp;

	if (strcmp(temp->word,root1->word)<0) 
	{
		root1->l = bstInsert(root1->l, temp);
		root1->l->p = root1;
	}
	else if (strcmp(temp->word,root1->word)>0)
	{
		root1->r = bstInsert(root1->r, temp);
		root1->r->p = root1;
	}
	return root1;
}

void rightRotate(struct node* temp)
{
	struct node* left = temp->l;
	temp->l = left->r;
	if (temp->l)
		temp->l->p = temp;
	left->p = temp->p;
	if (!temp->p)
		root = left;
	else if (temp == temp->p->l)
		temp->p->l = left;
	else
		temp->p->r = left;
	left->r = temp;
	temp->p = left;
}

void leftRotate(struct node* temp)
{
	struct node* right = temp->r;
	temp->r = right->l;
	if (temp->r)
		temp->r->p = temp;
	right->p = temp->p;
	if (!temp->p)
		root = right;
	else if (temp == temp->p->l)
		temp->p->l = right;
	else
		temp->p->r = right;
	right->l = temp;
	temp->p = right;
}

void fixBst(struct node* root, struct node* pt)
{
	struct node* parent_pt = NULL;
	struct node* grand_parent_pt = NULL;

	while ((pt != root) && (pt->c != 0)
		&& (pt->p->c == 1))
	{
		parent_pt = pt->p;
		grand_parent_pt = pt->p->p;

		if (parent_pt == grand_parent_pt->l)
		{

			struct node* uncle_pt = grand_parent_pt->r;

			if (uncle_pt != NULL && uncle_pt->c == 1)
			{
				grand_parent_pt->c = 1;
				parent_pt->c = 0;
				uncle_pt->c = 0;
				pt = grand_parent_pt;
			}

			else {

				if (pt == parent_pt->r) {
					leftRotate(parent_pt);
					pt = parent_pt;
					parent_pt = pt->p;
				}

				rightRotate(grand_parent_pt);
				int t = parent_pt->c;
				parent_pt->c = grand_parent_pt->c;
				grand_parent_pt->c = t;
				pt = parent_pt;
			}
		}

		else {
			struct node* uncle_pt = grand_parent_pt->l;

			if ((uncle_pt != NULL) && (uncle_pt->c == 1))
			{
				grand_parent_pt->c = 1;
				parent_pt->c = 0;
				uncle_pt->c = 0;
				pt = grand_parent_pt;
			}
			else {
				
				if (pt == parent_pt->l) {
					rightRotate(parent_pt);
					pt = parent_pt;
					parent_pt = pt->p;
				}

				leftRotate(grand_parent_pt);
				int t = parent_pt->c;
				parent_pt->c = grand_parent_pt->c;
				grand_parent_pt->c = t;
				pt = parent_pt;
			}
		}
	}

	root->c = 0;
}
struct node* newNode(char text[])
{
	struct node* temp = (struct node*)malloc(sizeof(struct node));
			temp->r = NULL;
			temp->l = NULL;
			temp->p = NULL;
			strcpy(temp->word,text);
			temp->c = 1;
			return temp;
}
int count(struct node* root)
{
    if(root==NULL)
        return 0;
    else
        return 1+count(root->l)+count(root->r);
}

int max(int x, int y)
{
    if(x>y)
        return x;
    else
        return y;
}
int height(struct node* root)
{
    if(root==NULL)
        return -1;
    else
        return 1+max(height(root->l),height(root->r));
}
struct node* search(struct node*root,char key[])
{
    if (root==NULL)
        return NULL;
    else if(strcasecmp(key,root->word)==0)
        return root;
    else if(strcasecmp(key,root->word)<0)
        return search(root->l,key);
    else
        return search(root->r,key);
}
void insertWord(char key[])
{
    struct node* n = search(root,key);
	struct node*temp;
    if (n == NULL)
        {
			temp = newNode(key);
            root=bstInsert(root,temp);
            fixBst(root,temp);
			FILE *f;
            f=fopen("Dictionary", "a");
   			if (f == NULL) {
      		  printf("Error!");
   			}
			else  fprintf(f,"\n%s",key);
			fclose(f);
			printf("Insertion Successful!\n\n");
		}
		else printf("ERROR: Word already in the dictionary!\n\n"); 
} 


int main()
{
        char text[50], text1[50], text2[50];
		int choice;
		struct node* temp;
    FILE *f;
    f=fopen("Dictionary", "r");
    if(f!=NULL)
    {
        printf("..................................\nDictionary Loaded Successfully...!\n..................................\n");
        while(!feof(f))
        {
            fscanf(f,"%s",text);
            temp = newNode(text);
            root=bstInsert(root,temp);
          	fixBst(root, temp);
        }
    }
    fclose(f);
        printf("Size = %d\n..................................\nHeight = %d\n..................................\n",count(root),height(root));
        while(choice!=3){
		printf("-------------\nPick a number\n-------------\n1- Search\n\n2- Insert\n\n3- Exit\n");
		scanf("%d",&choice);
		switch (choice)
		{
		case 1:
		printf("Search for: ");
        scanf("%s",text1);
		printf("\n");
        struct node* n = search(root,text1);
        if (n == NULL)
        {
            printf("NO, not found\n\n");
        }
        else printf("YES, found\n\n"); 
			break;
		case 2:
		printf("Enter word to insert: ");
        scanf("%s",text2);
		printf("\n");
        insertWord(text2);
		printf("Size after insertion= %d\n..................................\nHeight after insertion= %d\n..................................\n",count(root),height(root));
		}
		}	
	return 0;
}
